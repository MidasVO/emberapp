Project Ember
=========

Project Ember is a Rich Internet Application made with the MEAN stack.

Features
-----
  - Design your own Pokémon!
  - Share your Pokémon with friends!
  - 3D print your Pokémon!
  - Do battles!


Version
----

0.1 - It is still a work in progress.

Tech
-----------

Ember uses a number of open source projects to work properly:
-------------------------------------------------------------
  - [MongoDb] - NoSQL database
  - [Express] - fast node.js network app framework
  - [AngularJS] - HTML enhanced for web apps!
  - [node.js] - evented I/O for the backend
  - [Mean.js] - Open-Source Full-Stack Solution For MEAN Applications
  - [GRUNT] - The JavaScript Task Runner
  - [NPM] - Node Package Manager

Installation
--------------
The following steps should be executed to get the application running. This is step-by-step, if you just want to get the application running as fast as possible, scroll down for the quickstart commands.

##### Create the directory
This step is optional because when cloning the repository the folder gets created anyway.
```sh
mkdir emberapp
```

##### Clone the repository
Pull down all the files from the repository.
```sh
git clone https://bitbucket.org/MidasVO/emberapp .
```

##### Pull down dependencies
Node package manager installs all the dependencies for you with this command
```sh
npm install
```

##### Install Grunt if you have to
JavaScript Task Runner.
```sh
npm install -g grunt-cli
```

##### Start your mongodb instance (preferably in a screen)
It is required to have the database running before importing the data. It should preferably run in a screen instance because you're not going to be touching it later.
```sh
mongod
```

##### Install the mongo importer
We're going to need some extra tools to be able to import the json file.
```sh
sudo apt-get install mongodb-clients
```

##### Change to directory /db
```sh
cd db
```

##### Import the database files (outside of the shell environment)
The --jsonArray flag is important, if excluded it will not import the data properly. This command needs to be run outside of the shell invironment.
```sh
mongoimport --jsonArray  --collection pokemons --file emberdb.json
```

#### Changing the default port
The default port for the application is port 3000, there are several reasons why you'd want to change this. You can hardcode the port by editing '/config/env/all.js' or with the following command
```sh
export PORT=PORTNUMBER
```

### Run your application
Make sure you are in the root directory of your application and let the JavaScript Task Runner do its' job!
```sh
grunt
```

### Quit your application
Use the following key combination to quit the application
```sh
CTRL+C
```

Tests
-------

### Mocha
Run the mocha tests using this command in the root directory of the application
```sh
npm test
```

### Protractor
Protractor tests can be found at /home/emberApp/public/modules/core/protractor.js setup your protractor to read this file


### JSHint
JSHint runs with Grunt so no need to worry about that!

### API Tests
#### http://server3.tezzt.nl:3000/#/execute/538ec907f2e5ce0f079cb1e1
#### login with email: mozartinc@gmail.com
#### Run the Pokemons scenario

License
----

MIT
[node.js]:http://nodejs.org
[express]:http://expressjs.com
[MongoDB]:http://mongodb.org
[AngularJS]:https://angularjs.org/
[Mean.js]:http://meanjs.org
[GRUNT]:http://gruntjs.com/
[NPM]:https://www.npmjs.org/