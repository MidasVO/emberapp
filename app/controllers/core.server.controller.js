'use strict';

/**
 * Render the index
 * @param  {[type]} req
 * @param  {[type]} res
 * @return {[type]}
 */
exports.index = function(req, res) {
	res.render('index', {
		user: req.user || null
		
	});
	console.log(req.user);
};