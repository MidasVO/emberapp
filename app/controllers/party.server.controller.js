'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Pokemons = mongoose.model('Pokemons'),
    _ = require('lodash');

/**
 * Show the part of the current user.
 * @param  {Object} req
 * @param  {Object} res
 * @return pokemon
 */
exports.read = function(req, res) {
	
	console.log(
			'Trying to read party.'
		);
	var userId = req._id._id;
	var myParty;
	console.log('User ' + req._id.name + ' has an ID of ' + userId);
		console.log(
			'Reading party intensifies..'
		);
	Pokemons.find({'ownerId':userId}).exec(function(err, pokemon) {
		req.pokemon = pokemon;
		res.jsonp(req.pokemon);

	});
};
