'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	passport = require('passport'),
	Pokemons = mongoose.model('Pokemons'),
	Users = mongoose.model('User'),
	_ = require('lodash');

/**
 * Get error message from the error object.
 * @param  {Object} err
 * @return message
 */
var getErrorMessage = function(err) {
	var message = '';

	if (err.code) {
		switch (err.code) {
			case 11000:
			case 11001:
				message = 'Username already exists';
				break;
			default:
				message = 'Something went wrong';
		}
	} else {
		for (var errName in err.errors) {
			if (err.errors[errName].message) message = err.errors[errName].message;
		}
	}

	return message;
};
/**
 * List all the pokemon.
 * @param  {Object} req
 * @param  {Object} res
 * @return pokemons
 */
exports.list = function(req, res) {
	Pokemons.find().exec(function(err, pokemons) {

		if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
			// var obj = {"JSObject" : pokemons};
			// pokemons = JSON.stringify(obj);
			res.jsonp(pokemons);
		}
	});
};
/**
 * Gets the pokemon by the ID.
 * @param  {Object}   req
 * @param  {Object}   res
 * @param  {Function} next
 * @param  {String}   id
 * @return pokemon
 */
exports.getPokemonById = function(req, res, next, id) {
	Pokemons.findOne({
		_id: id
	}).exec(function(err, pokemon) {
		if (err) return next(err);
		if (!pokemon) return next(new Error('Failed to load User ' + id));
		req.pokemon = pokemon;
		next();
	});
};
/**
 * Gets pokemon and orders them by votes descending.
 * @param  {Object} req
 * @param  {Object} res
 * @return {Object}
 */
exports.getTopPokemon = function(req, res) {
	Pokemons.find().sort([['votes', 'descending']]).exec(function(err, pokemons) {

		if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
			// var obj = {"JSObject" : pokemons};
			// pokemons = JSON.stringify(obj);
			res.jsonp(pokemons);
		}
	});
};
/**
 * Gets the top {amount} pokemon and orders them by votes descending.
 * @param  {Object} req
 * @param  {Object} res
 * @param  {Number} amount
 * @return pokemons
 */
exports.getTop = function(req, res, amount) {
	Pokemons.find().sort([['votes', 'descending']]).limit(amount).exec(function(err, pokemons) {

		if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
			// var obj = {"JSObject" : pokemons};
			// pokemons = JSON.stringify(obj);
			res.jsonp(pokemons);
		}
	});
};
/**
 * Gets the pokemon by name, limits to 1.
 * @param  {Object}   req
 * @param  {Object}   res
 * @param  {Function} next
 * @param  {String}   name
 * @return pokemon
 */
exports.getPokemonByName = function(req, res, next, name) {
	Pokemons.find({'name':name}).limit(1).populate('user', 'displayName').exec(function(err, pokemon) {
		if (err) return next(err);
		if (!pokemon) return next(new Error('Failed to load pokemon ' + name));
		req.pokemon = pokemon;
		next();
	});
};
/**
 * Gets the pokemon by gender, limits to 1.
 * @param  {Object}   req
 * @param  {Object}   res
 * @param  {Function} next
 * @param  {String}   gender
 * @return pokemon
 */
exports.getPokemonByGender = function(req, res, next, gender) {
	Pokemons.find({'gender':gender}).limit(1).populate('user', 'displayName').exec(function(err, pokemon) {
		if (err) return next(err);
		if (!pokemon) return next(new Error('Failed to load pokemon ' + gender));
		req.pokemon = pokemon;
		next();
	});
};
/**
 * Gets the pokemons by ownerId
 * @param  {Object}   req
 * @param  {Object}   res
 * @param  {Function} next
 * @param  {String}   ownerId
 * @return pokemon
 */exports.getPokemonsByOwnerId = function(req, res, next, ownerId) {
	Pokemons.find({'ownerId':ownerId}).exec(function(err, pokemon) {
		if (err) return next(err);
		if (!pokemon) return next(new Error('Failed to load pokemon ' + ownerId));
		req.pokemon = pokemon;
		next();
	});
};

/**
 * Read the pokemon object
 * @param  {Object} req
 * @param  {Object} res
 */
exports.read = function(req, res) {
	console.log(req.params);
	res.jsonp(req.pokemon);
};
