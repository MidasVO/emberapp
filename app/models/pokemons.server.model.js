'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Article Schema
 */
var PokemonsSchema = new Schema({

	name: {
		type: String
	},
	ownerId: {
		type: Schema.Types.ObjectId, ref: 'User'	
	},
	elementType: {
		type: String
	},
	bodyType: {
		type: String
	},
	height: {
		type: Number
	},
	weight: {
		type: Number
	},
	species: {
		type: String
	},
	description: {
		type: String
	},
	moves: {
		first: String,
		second: String,
		third: String,
		fourth: String
	},
	eggGroups: {
		first: String,
		second: String
	},
	gender: {
		type: String
	},
	eggCycles: {
		type: Number
	},
	growthRate: {
		type: Number
	},
	hp: {
		type: Number
	},
	attack: {
		type: Number
	},
	defense: {
		type: Number
	},
	spAtk: {
		type: Number
	},
	spDef: {
		type: Number
	},
	speed: {
		type: Number
	},
	preEvolve: {
		type: Number
	},
	postEvolve: {
		type: Number
	},
	imageID: {
		type: String
	},
	growl: {
		type: String
	},
	votes: {
		type: Number
	},
	wins: {
		type: Number
	},
	losses: {
		type: Number
	},
	draws: {
		type: Number
	}

});

//TODO
// RETURN TOP 5 /25

//

mongoose.model('Pokemons', PokemonsSchema);