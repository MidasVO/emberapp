'use strict';
var users = require('../../app/controllers/users'),
	pokemons = require('../../app/controllers/pokemons'),
	party = require('../../app/controllers/party');

module.exports = function(app) {
	// Routing logic   
	app.route('/pokemons')
		.get(pokemons.list);	

	app.route('/pokemon/:name')
		.get(pokemons.read);
	app.route('/party')
		.get(pokemons.list);
	app.route('/party/:username')
		.get(party.read);
	app.route('/:username/party')
		.get(party.read);

	app.route('/pokemons/top')
		.get(pokemons.getTopPokemon);
	app.route('/pokemons/top/:amount')
		.get(pokemons.getTopPokemon);
	app.param('name', pokemons.getPokemonByName);
	app.param('ownerId', pokemons.getPokemonsByOwnerId);
	app.param('amount', pokemons.getTop);
	app.param('username', users.getUserId);
};

