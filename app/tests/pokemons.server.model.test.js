'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Pokemons = mongoose.model('Pokemons');

/**
 * Globals
 */
var user, pokemons;

/**
 * Unit tests
 */
describe('Pokemons Model Unit Tests:', function() {
	beforeEach(function(done) {
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: 'username',
			password: 'password'
		});

		user.save(function() { 
			pokemons = new Pokemons({
			name: 'PokemonName',
			ownerId: '5383091bc968e925dc3c5576',
			elementType: 'elementType',
			bodyType: 'bodyType',
			height: 10,
			weight: 10,
			species: 'theseSpecies',
			description: 'a neat pokemon',
			eggGroups: {
				first: 'Egg1',
				second: 'Egg2',
			},
			gender: 'm',
			eggCycles: 3,
			growthRate: 2,
			hp: 10,
			attack: 5,
			deffense: 5,
			spAtk: 12,
			spDef: 13,
			speed: 2,
			preEvolve: 1,
			postEvolve: 3,
			imageID: 'dmew9jd9d9wekdw9kedkw9edk9wekd',
			growl: '3e93dk39d3k9dk39dk3d',
			votes: 12,
			wins: 12,
			losses: 3,
			draws: 12
			});

			done();
		});
	});

	describe('Method create', function() {
		it('should be able to create pokemon without problems', function(done) {
			return pokemons.save(function(err) {
				should.not.exist(err);
				done();
			});
		});
	});
	describe('Method update', function() {
		it('should be able to update pokemon without problems', function(done) {
			return pokemons.update(function(err) {
				should.not.exist(err);
				done();
			});
		});
	});
	describe('Method remove', function() {
		it('should be able to remove pokemon without problems', function(done) {
			return Pokemons.remove(function(err) {
				should.not.exist(err);
				done();
			});
		});
	});
	afterEach(function(done) { 
		Pokemons.remove().exec();
		User.remove().exec();
		
		done();
	});
});