'use strict';

module.exports = {
	app: {
		title: 'EmberApp',
		description: 'Create and design your own pokemon',
		keywords: 'pokemon, design, create, share'
	},
	port: process.env.PORT || 3000,
	templateEngine: 'swig',
	sessionSecret: 'MEAN',
	sessionCollection: 'sessions',
	assets: {
		lib: {
			css: [
				'public/lib/ember/css/normalize.css',
				'public/lib/ember/css/project-ember.webflow.css',
				'public/lib/ember/css/webflow.css'
			],
			js: [
				'public/lib/angular/angular.js',
				'public/lib/jquery/dist/jquery.js',
				'public/lib/angular-resource/angular-resource.js', 
				'public/lib/angular-cookies/angular-cookies.js', 
				'public/lib/angular-animate/angular-animate.js', 
				'public/lib/angular-touch/angular-touch.js', 
				'public/lib/angular-sanitize/angular-sanitize.js', 
				'public/lib/angular-ui-router/release/angular-ui-router.js',
				'public/lib/angular-ui-utils/ui-utils.js',
				'public/lib/angular-bootstrap/ui-bootstrap-tpls.js',
				'public/lib/ember/js/audiodisplay.js',
				'public/lib/ember/js/recorderjs/recorder.js',
				'public/lib/ember/js/recorderjs/recorderWorker.js',
				'public/lib/ember/js/main.js',
				'public/lib/ember/js/editor.js',
				'public/lib/ember/js/webflow.js',
				'public/lib/ember/js/modernizr.js'

			]
		},
		css: [
			'public/modules/**/css/*.css'
		],
		js: [
			'public/config.js',
			'public/application.js',
			'public/modules/*/*.js',
			'public/modules/*/*[!tests]*/*.js',

		],
		tests: [
			'public/lib/angular-mocks/angular-mocks.js',
			'public/modules/*/tests/*.js'
		]
	}
};