// conf.js
// @see https://github.com/angular/protractor/blob/master/referenceConf.js
//
exports.config = {
    // The address of a running selenium server. If specified, Protractor will
    // connect to an already running instance of selenium. This usually looks like
    // seleniumAddress: 'http://localhost:4444/wd/hub'
    seleniumAddress: 'http://localhost:4444/wd/hub',

    // ----- Capabilities to be passed to the webdriver instance ----
    //
    // For a full list of available capabilities, see
    // https://code.google.com/p/selenium/wiki/DesiredCapabilities
    // and
    // https://code.google.com/p/selenium/source/browse/javascript/webdriver/capabilities.js
    capabilities: {
        'browserName': 'firefox' // firefox | phantomjs | chrome | safari
    },

    // ----- What tests to run -----
    //
    // Spec patterns are relative to the location of this config.
    specs: ['specs.js'],

    // ----- Options to be passed to minijasminenode -----
    //
    // See the full list at https://github.com/juliemr/minijasminenode
    jasmineNodeOpts: {
        showColors: true,               // If true, print colors to the terminal.
        isVerbose: true,                // If true, display spec names.
        includeStackTrace: true,        // If true, include stack traces in failures.
        defaultTimeoutInterval: 30000   // Default time to wait in ms before a test fails.

    }
}