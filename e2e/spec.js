var result = element(by.id('result'));
var elementFire = element(by.id('elementFire'));
var elementElectric = element(by.id('elementElectric'));
var elementWater = element(by.id('elementWater'));
var elementSteel = element(by.id('elementSteel'));
var elementGrass = element(by.id('elementGrass'));
var elementFight = element(by.id('elementFight'));
var elementPsychic = element(by.id('elementPsychic'));
var elementDark = element(by.id('elementDark'));

it('should check result color on elementFire click', function() {
  expect(result.getCssValue('background-color')).toBe('rgb(255, 203, 92)');
  elementFire.click();
  expect(result.getCssValue('background-color')).toBe('rgb(239, 127, 46)');
});

it('should check result color on elementElectric click', function() {
  elementElectric.click();
  expect(result.getCssValue('background-color')).toBe('rgb(247, 208, 49)');
});

it('should check result color on elementWater click', function() {
  elementWater.click();
  expect(result.getCssValue('background-color')).toBe('rgb(124, 177, 214)');
});

it('should check result color on elementSteel click', function() {
  elementSteel.click();
  expect(result.getCssValue('background-color')).toBe('rgb(162, 149, 133)');
});

it('should check result color on elementGrass click', function() {
  elementGrass.click();
  expect(result.getCssValue('background-color')).toBe('rgb(143, 169, 60)');
});

it('should check result color on elementFight click', function() {
  elementFight.click();
  expect(result.getCssValue('background-color')).toBe('rgb(168, 48, 35)');
});

it('should check result color on elementPsychic click', function() {
  elementPsychic.click();
  expect(result.getCssValue('background-color')).toBe('rgb(151, 103, 159)');
});

it('should check result color on elementDark click', function() {
  elementDark.click();
  expect(result.getCssValue('background-color')).toBe('rgb(95, 76, 91)');
});