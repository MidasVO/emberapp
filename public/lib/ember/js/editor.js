/*(function(){
	"use strict";
	var angular,
		app = angular.module('editor', []),
  		module= {

  		// draw the colorwheel image on the canvas, needs element name in string format example: "fire" and the ID of the canvas in the HTML
  		drawColorwheel: function(canvasID, element){
  			var document,
  				canvas = document.getElementById(canvasID),
				centerX = canvas.width/2,
				centerY = canvas.height/2,
				ctx = canvas.getContext('2d'),
				img = new Image();
			img.src = "images/colorpicker"+element+".svg";
			img.onload = function(){
				ctx.drawImage(img,0,0,100,100);
				ctx.arc(centerX, centerY, 20, 0, Math.PI*2);
				//ctx.fillStyle = 'green';
				//ctx.fill();
			};
  		},

  		// find the position of the mouse
		findPos: function (obj) {
			var curleft = 0, curtop = 0;
			if (obj.offsetParent) {
					do {
							curleft += obj.offsetLeft;
							curtop += obj.offsetTop;
					} while (obj = obj.offsetParent);
					return { x: curleft, y: curtop };
			}
			return undefined;
		},

		// converts rgb values to hex string
		rgbToHex: function (r, g, b) {
			if (r > 255 || g > 255 || b > 255)
					throw "Invalid color component";
			return ((r << 16) | (g << 8) | b).toString(16);
		},

		//initiates the colorPicker, colorwheelID is the HTML canvasID based on this ID the targeted color will be dertimened (primary, secondary or tetriary)
		colorPicker: function (canvasID, element){
			editor.drawColorwheel(canvasID, element);
			//sets the color of the circle in the middle of the colorwheel to the color of the the pixel the mouse is on
			$('#'+canvasID).mousemove(function(e) {
				var pos = editor.findPos(this);
				var x = e.pageX - pos.x;
				var y = e.pageY - pos.y;
				var coord = "x=" + x + ", y=" + y;
				var c = this.getContext('2d');
				var p = c.getImageData(x, y, 1, 1).data; 
				var hex = "#" + ("000000" + editor.rgbToHex(p[0], p[1], p[2])).slice(-6);
				if (hex != "#000000"){
					c.fillStyle = hex;
					c.fill();
					console.log(hex);
				}
			});
		}

		init: function(){

  			var window;

  			// draw the three colorpickers
  			function initPickers (){
					editor.colorPicker("colorwheelPrimary", "fire");
					editor.colorPicker("colorwheelSecondary", "fire");
					editor.colorPicker("colorwheelTetriary", "fire");
					console.log("pickers loaded");
				};

			window.onload = initPickers;
  		},
  };

  window.editor=module;
}());

editor.init();*/