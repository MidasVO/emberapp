'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {
		// Redirect to home view when route not found
		$urlRouterProvider.otherwise('/');

		// Home state routing
		$stateProvider.
		state('editor', {
			url: '/editor',
			templateUrl: 'modules/core/views/editor.client.view.html'
		}).
		state('leaderboard', {
			url: '/leaderboard',
			templateUrl: 'modules/core/views/leaderboard.client.view.html'
		}).
		state('arena', {
			url: '/arena',
			templateUrl: 'modules/core/views/arena.client.view.html'
		}).
		state('dashboard', {
			url: '/dashboard',
			templateUrl: 'modules/core/views/dashboard.client.view.html'
		}).
		state('party', {
			url: '/party',
			templateUrl: 'modules/core/views/party.client.view.html'
		}).
		state('home', {
			url: '/',
			templateUrl: 'modules/core/views/home.client.view.html'
		});
	}
]);