/* 
When linting this code be sure to tell jslint to ignore undefined values like console,
errors caused by this are intentional and will not break the code.
Also we do not lint messy whitespaces.
*/

var angular;

angular.module('core').controller('ArenaController', ['$scope', '$http', '$stateParams', '$location', 'Authentication', 'Tests',
	function($scope, $http, Authentication) {
	'use strict';
	$scope.authentication = Authentication;
	$scope.formData = {};

	// when landing on the page, get all pokemons and show them
	$http.get('/users/me')
		.success(function(data) {
			$scope.users = data;
			console.log(data);
		})
		.error(function(data) {
			console.log('Error: ' + data);
		});
	}
]);