/* 
When linting this code be sure to tell jslint to ignore undefined values like console,
errors caused by this are intentional and will not break the code.
Also we do not lint messy whitespaces.
*/

var angular;

angular.module('core').controller('EditorController', ['$scope', function($scope) {
		'use strict';
		$scope.elementType = null;
		$scope.BodyType = null;
		$scope.primaryColor = null;
		$scope.secondaryColor = null;
		$scope.tetriaryColor = null;
		//Background color used for background colors that need to be changed when the elementType changes
		$scope.backgroundColor = {'background-color':'#ffcb5c'};

		//elementType click handler sets the elementType and the backgroundColor
		this.setElementType = function(element){
			$scope.elementType = element;

			switch (element){
				case 'fire':
					$scope.backgroundColor = {'background-color':'#ef7f2e'};
					break;
				case 'electricity':
					$scope.backgroundColor = {'background-color':'#f7d031'};
					break;
				case 'water':
					$scope.backgroundColor = {'background-color':'#7cb1d6'};
					break;
				case 'steel':
					$scope.backgroundColor = {'background-color':'#a29585'};
					break;
				case 'grass':
					$scope.backgroundColor = {'background-color':'#8fa93c'};
					break;
				case 'fight':
					$scope.backgroundColor = {'background-color':'#a83023'};
					break;
				case 'psychic':
					$scope.backgroundColor = {'background-color':'#97679f'};
					break;
				case 'dark':
					$scope.backgroundColor = {'background-color':'#5f4c5b'};
					break;
				default:
					console.log('Matching background-color not found for '+element+' reverting to default color #ffcb5c');
					$scope.backgroundColor = {'background-color':'#ffcb5c'};
			}
		};

		/*
		The following is code added by the designers in order to improve the design of the website.
		This code is not and will not be tested. for the sake of keeping the site valid for testing this code
		will be commented out until the end presentation.
		*/
		
		//This adds and removes active states in the elementsection.
		$(function(){
			$('.iconbackground.vuur').on('click', function(){
				$(this).addClass('active');
				$('.iconbackground.fight').removeClass('active');
				$('.iconbackground.water').removeClass('active');
				$('.iconbackground.steel').removeClass('active');
				$('.iconbackground.grass').removeClass('active');
				$('.iconbackground.electricity').removeClass('active');
				$('.iconbackground.psychic').removeClass('active');
				$('.iconbackground.dark').removeClass('active');
				// Zoeken naar wat beneden actief is.
			});
		});

		$(function(){
			$('.iconbackground.electricity').on('click', function(){
				$(this).addClass('active');
				$('.iconbackground.vuur').removeClass('active');
				$('.iconbackground.water').removeClass('active');
				$('.iconbackground.steel').removeClass('active');
				$('.iconbackground.grass').removeClass('active');
				$('.iconbackground.fight').removeClass('active');
				$('.iconbackground.psychic').removeClass('active');
				$('.iconbackground.dark').removeClass('active');
			});
		});

		$(function(){
			$('.iconbackground.water').on('click', function(){
				$(this).addClass('active');
				$('.iconbackground.vuur').removeClass('active');
				$('.iconbackground.electricity').removeClass('active');
				$('.iconbackground.steel').removeClass('active');
				$('.iconbackground.grass').removeClass('active');
				$('.iconbackground.fight').removeClass('active');
				$('.iconbackground.psychic').removeClass('active');
				$('.iconbackground.dark').removeClass('active');
			});
		});

		$(function(){
			$('.iconbackground.steel').on('click', function(){
				$(this).addClass('active');
				$('.iconbackground.vuur').removeClass('active');
				$('.iconbackground.water').removeClass('active');
				$('.iconbackground.electricity').removeClass('active');
				$('.iconbackground.grass').removeClass('active');
				$('.iconbackground.fight').removeClass('active');
				$('.iconbackground.psychic').removeClass('active');
				$('.iconbackground.dark').removeClass('active');
			});
		});

		$(function(){
			$('.iconbackground.grass').on('click', function(){
				$(this).addClass('active');
				$('.iconbackground.vuur').removeClass('active');
				$('.iconbackground.water').removeClass('active');
				$('.iconbackground.steel').removeClass('active');
				$('.iconbackground.electricity').removeClass('active');
				$('.iconbackground.fight').removeClass('active');
				$('.iconbackground.psychic').removeClass('active');
				$('.iconbackground.dark').removeClass('active');
			});
		});


		$(function(){
			$('.iconbackground.fight').on('click', function(){
				$(this).addClass('active');
				$('.iconbackground.vuur').removeClass('active');
				$('.iconbackground.water').removeClass('active');
				$('.iconbackground.steel').removeClass('active');
				$('.iconbackground.grass').removeClass('active');
				$('.iconbackground.electricity').removeClass('active');
				$('.iconbackground.psychic').removeClass('active');
				$('.iconbackground.dark').removeClass('active');
			});
		});

		$(function(){
			$('.iconbackground.psychic').on('click', function(){
				$(this).addClass('active');
				$('.iconbackground.vuur').removeClass('active');
				$('.iconbackground.water').removeClass('active');
				$('.iconbackground.steel').removeClass('active');
				$('.iconbackground.grass').removeClass('active');
				$('.iconbackground.fight').removeClass('active');
				$('.iconbackground.electricity').removeClass('active');
				$('.iconbackground.dark').removeClass('active');
			});
		});

		$(function(){
			$('.iconbackground.dark').on('click', function(){
				$(this).addClass('active');
				$('.iconbackground.vuur').removeClass('active');
				$('.iconbackground.water').removeClass('active');
				$('.iconbackground.steel').removeClass('active');
				$('.iconbackground.grass').removeClass('active');
				$('.iconbackground.fight').removeClass('active');
				$('.iconbackground.psychic').removeClass('active');
				$('.iconbackground.electricity').removeClass('active');
			});
		});




		/*this.drawColorwheel = function(canvasID, element){
			window.onload = function(){
				var canvas = angular.element(canvasID);
				var centerX = canvas.width/2;
				var centerY = canvas.height/2;
				var ctx = canvas.getContext('2d');
				var img = new Image();
				img.src = "images/colorpicker"+element+".svg";
				img.onload = function(){
					ctx.drawImage(img,0,0,100,100);
					ctx.arc(centerX, centerY, 20, 0, Math.PI*2);
					//ctx.fillStyle = 'green';
					//ctx.fill();
				};
			};
		};*/
	}
]);

/*(function(){
	var app = angular.module('editor',[]);
	var module= {

  		init: function(){

  			// draw the three colorpickers
  			window.onload = initPickers;
  			function initPickers (){
					editor.colorPicker("colorwheelPrimary", "fire");
					editor.colorPicker("colorwheelSecondary", "fire");
					editor.colorPicker("colorwheelTetriary", "fire");
					console.log("pickers loaded");
				}
  		},

  		// draw the colorwheel image on the canvas, needs element name in string format example: "fire" and the ID of the canvas in the HTML
  		drawColorwheel: function(canvasID, element){
  			var canvas = document.getElementById(canvasID);
			var centerX = canvas.width/2;
			var centerY = canvas.height/2;
			var ctx = canvas.getContext('2d');
			var img = new Image();
			img.src = "images/colorpicker"+element+".svg";
			img.onload = function(){
				ctx.drawImage(img,0,0,100,100);
				ctx.arc(centerX, centerY, 20, 0, Math.PI*2);
				//ctx.fillStyle = 'green';
				//ctx.fill();
			};
  		},

  		// find the position of the mouse
		findPos: function (obj) {
			var curleft = 0, curtop = 0;
			if (obj.offsetParent) {
					do {
							curleft += obj.offsetLeft;
							curtop += obj.offsetTop;
					} while (obj = obj.offsetParent);
					return { x: curleft, y: curtop };
			}
			return undefined;
		},

		// converts rgb values to hex string
		rgbToHex: function (r, g, b) {
			if (r > 255 || g > 255 || b > 255)
					throw "Invalid color component";
			return ((r << 16) | (g << 8) | b).toString(16);
		},

		//initiates the colorPicker, colorwheelID is the HTML canvasID based on this ID the targeted color will be dertimened (primary, secondary or tetriary)
		colorPicker: function (canvasID, element){
			editor.drawColorwheel(canvasID, element);
			//sets the color of the circle in the middle of the colorwheel to the color of the the pixel the mouse is on
			$('#'+canvasID).mousemove(function(e) {
				var pos = editor.findPos(this);
				var x = e.pageX - pos.x;
				var y = e.pageY - pos.y;
				var coord = "x=" + x + ", y=" + y;
				var c = this.getContext('2d');
				var p = c.getImageData(x, y, 1, 1).data; 
				var hex = "#" + ("000000" + editor.rgbToHex(p[0], p[1], p[2])).slice(-6);
				if (hex != "#000000"){
					c.fillStyle = hex;
					c.fill();
					console.log(hex);
				}
			}),

			$('#'+canvasID).click(function(e) {
				var preview = document.getElementById("result");
				var pos = editor.findPos(this);
				var x = e.pageX - pos.x;
				var y = e.pageY - pos.y;
				var coord = "x=" + x + ", y=" + y;
				var c = this.getContext('2d');
				var p = c.getImageData(x, y, 1, 1).data; 
				var hex = "#" + ("000000" + editor.rgbToHex(p[0], p[1], p[2])).slice(-6);
				if (hex != "#000000"){
					preview.style.background = hex;
					console.log(hex);
				}
			});
		}
  };

  window.editor=module;
}());

editor.init();*/