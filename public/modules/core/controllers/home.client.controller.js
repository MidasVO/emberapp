/* 
When linting this code be sure to tell jslint to ignore undefined values like console,
errors caused by this are intentional and will not break the code.
Also we do not lint messy whitespaces.
*/

var angular;

angular.module('core').controller('HomeController', ['$scope', 'Authentication',
	function($scope, Authentication) {
		'use strict';
		// This provides Authentication context.
		$scope.authentication = Authentication;
	}
]);