/* 
When linting this code be sure to tell jslint to ignore undefined values like console,
errors caused by this are intentional and will not break the code.
Also we do not lint messy whitespaces.
*/

var angular;

// Leaderboard controller
angular.module('core').controller('LeaderboardController', ['$scope', '$http', '$stateParams', '$location', 'Authentication', 'Tests',
	function($scope, $http, Authentication) {
	'use strict';

	$scope.authentication = Authentication;
	$scope.formData = {};

	// when landing on the page, get all pokemons and show them
	$http.get('/pokemons/top')
		.success(function(data) {
			$scope.pokemons = data;
			console.log(data);
		})
		.error(function(data) {
			console.log('Error: ' + data);
		});
	// when landing on the page, get the user and show them
	$http.get('/pokemons/top')
		.success(function(data) {
			$scope.pokemons = data;
			console.log(data);
		})
		.error(function(data) {
			console.log('Error: ' + data);
		});

	// when landing on the page, get all pokemons and show them
	$http.get('/users/me')
		.success(function(data) {
			$scope.users = data;
			console.log(data);
		})
		.error(function(data) {
			console.log('Error: ' + data);
		});



	}
]);