/* 
When linting this code be sure to tell jslint to ignore undefined values like console,
errors caused by this are intentional and will not break the code.
Also we do not lint messy whitespaces.
*/

var angular;

angular.module('core').controller('PartyController', ['$scope', '$http', '$stateParams', '$location', 'Authentication', 'Tests',
	function($scope, $http, Authentication) {
	'use strict';
	
	$scope.authentication = Authentication;
	$scope.formData = {};
	console.log($scope.username);
	// get the currently logged in user
	$http.get('/users/me')
		.success(function(data) {
			$scope.users = data;
			console.log(data);
		// get their party
		$http.get('/party/' + data.name)
			.success(function(data) {
				$scope.pokemons = data;
				console.log(data);
			})
			.error(function(data) {
				console.log('Error: ' + data);
			});
		})
		.error(function(data) {
			console.log('Error: ' + data);
		});

	}
]);