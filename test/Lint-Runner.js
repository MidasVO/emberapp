var LintRoller = require('./node_modules/lintroller/');

var config = {
    verbose          : false,
    stopOnFirstError : false,

    //recursively include JS files in these folders
    filepaths  : [
        '../public/lib/ember/js'
    ],

    //but ignore anything in these folders
    exclusions : [
        './node_modules/',
        '../../client/javascript/lib/'
    ],

    linters : [
        {
            type : 'jslint'
        },
        {
            type : 'jshint'
        },
        {
            type : 'esprima'
        }
    ],

    stdoutErrors: true
};

try {
    LintRoller.init(config);
}
catch (e) {
    console.log('\nAn error has been caught:\n\n');
    console.log(e);
    process.exit(1);
}